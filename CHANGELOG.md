# Changelog

We try to follow semantic versioning.

## [Unreleased]
### Added
### Changed
### Deprecated
### Fixed
### Removed
### Security

## [0.1.0] - 2020-06-12

- First beta release providing a Quota model that is actually usable.
