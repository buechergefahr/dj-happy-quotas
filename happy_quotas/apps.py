from django.apps import AppConfig


class HappyQuotasConfig(AppConfig):
    name = 'happy_quotas'
